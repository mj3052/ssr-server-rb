#!/usr/bin/env ruby -w
require "socket"
require 'sinatra'
require 'json'

set :environment, :production

class Server

  def initialize( port )
    @server = TCPServer.new port
    $connections = Hash.new
    $devices = Hash.new
    @clients = Hash.new
    $connections[:server] = @server
    $connections[:clients] = @clients
    run
  end

  def run
    # We want to be able to kill the server when sending  the INT signal (ctrl+c)
    Signal.trap('INT') { exit 0 }
    puts "TCP server is listening"
    loop {
      Thread.start(@server.accept) do | client |
        name = client.gets.chomp.to_sym
        id = name.to_s.delete(' ').to_sym

        if $connections[:clients].count == 0
          newDevice(id, name)
        end

        $connections[:clients].each do |other_name, other_client|
          if id == other_name || client == other_client
            puts "Client already added as connection, not adding to devices."
          else
              newDevice(id, name)
          end
        end

        
        $connections[:clients][id] = client
        client.puts "Connection accepted"
        listen_user_messages( id, client )
        client.puts "1"
      end
    }.join
  end

  def newDevice(id, name)
    puts "Adding new device to devices"
    $devices[id] = {:name => name.to_s, :status => "0", :id => id}
  end

  def updateStatus(name, status)
    puts "Updating status"
    $devices[name][:status] = status
  end
 
  def listen_user_messages( username, client )
    loop {
      puts "Opened stream"
      msg = client.gets.chomp
      parse = msg.to_s.delete(' ').split(",")

      if parse.count > 1
        if parse[0] == "message"
          puts "Message received: #{parse[1]} | From: #{username}"
        elsif parse[0] == "status"
          puts "Status update received: #{parse[1]} | From: #{username}"
            updateStatus(username, parse[1])
        end
      end

=begin
      $connections[:clients].each do |other_name, other_client|
        unless other_name == username
          other_client.puts "#{username.to_s}: #{msg}"
        end
      end
=end
    }
  end
end

sinatra_thread = Thread.new do
  class SinatraServer < Sinatra::Application
 
    puts "Sinatra running in thread: #{Thread.current}"
 
    class << self
      attr_reader :sinatra_thread
    end
 
    get '/debug' do
      print "Connections object: "
      p $connections
      "Done."
    end

    get '/list' do
      content_type :json
      # Create a nice JSON response with a list of connected devices
      response = {"devices" => {"dict" => $devices, "list" => $devices.values}}
      response.to_json
    end

    get '/device/:id/:command' do
       content_type :json

        command = params[:command]
        id = params[:id].to_sym
        print "Device: "
        p id
        print "Command: "
        p command

        if $connections[:clients].has_key?(id)

            if command == "on"
                puts "Turning on device"
                $connections[:clients][id].puts "command, on"
            elsif command == "off"
                puts "Turning off device"
                $connections[:clients][id].puts "command, off"
            elsif command == "status"
                puts "Updating device status"
                $connections[:clients][id].puts "command, status"
            elsif command == "ping"
                puts "Pinging device"
                $connections[:clients][id].puts "command, ping"
            else
                return {"status" => "error", "message" => "Unknown command"}.to_json
            end

            {"status" => "success", "message" => ""}.to_json

        else
            {"status" => "error", "message" => "Unknown device"}.to_json
        end
    end
 
    get '/exit' do
      exit!(0)
    end
 
    run!
  end
end

server_thread = Thread.new { Server.new( 2000 ) }

puts "Threads have been created"

threads = [sinatra_thread, server_thread]

puts "Threads have been joined. Application is running."
threads.each { |thread| thread.join }

# Nothing gets done past this point
